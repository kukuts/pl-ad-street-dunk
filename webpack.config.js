const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const webpackConfig = {
	context: __dirname,
	output: {
		path: path.resolve(__dirname, "bin"),
		filename: "js/[name].js"
	},
	entry: {
		pixi: "./node_modules/pixi.js",
		main: "./src/app.ts"
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: "ts-loader",
				exclude: /node_modules/,
			},
		]
	},
	devtool: "source-map",
	plugins: [
		new HtmlWebpackPlugin({
			inlineSource: ".(js|css)$",
			title: "Street dunk",
			template: "template/index.html",
			filename: "index.html",
			hash: false,
		}),
		new HtmlWebpackInlineSourcePlugin(),
		new UglifyJsPlugin({
			sourceMap: false,
			extractComments: false
		})
	],
	resolve: {
		extensions: [".js", ".ts", ".css", ".json"],
		modules: ["node_modules"],
		alias: {
			"assets": path.join(__dirname, "./assets")
		}
	},
};
module.exports = () => webpackConfig;
