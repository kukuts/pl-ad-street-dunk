import {GameApp} from "./game/GameApp";
import {DAPIApp} from "./ad/ironSource/DAPIApp";
import {DummyApp} from "./ad/dummy/DummyApp";

const isLocalhost: boolean = window.location.hostname === "";

new GameApp(isLocalhost ? new DummyApp() : new DAPIApp());