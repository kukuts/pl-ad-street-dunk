import {IAdSizeState} from "../interfaces";
import {GameApp} from "../../game/GameApp";
import {AdApp} from "../AdApp";

export class DummyApp extends AdApp {

	constructor() {
		super();
		window.addEventListener("resize", this._onWindowResize.bind(this));
	}

	public getScreenSize(): IAdSizeState {
		const view: HTMLCanvasElement = GameApp.pixi.view;
		return {
			width: view.clientWidth,
			height: view.clientHeight
		}
	}

	public isAudioEnabled(): boolean {
		return false;
	}

	public isReady(): boolean {
		return true;
	}

	public isViewable(): boolean {
		return true;
	}

	public onButtonCTAClick(data?: any): void {
		if (this._onViewableChangeCallback) {
			this._onViewableChangeCallback({
				visible: false
			});
			console.log("Dummy ad: isViewable false");
			setTimeout(() => {
				if (this._onViewableChangeCallback) {
					this._onViewableChangeCallback({
						visible: true
					});
					console.log("Dummy ad: isViewable true");
				}
			}, 1000);
		}
	}

	private _onWindowResize(): void {
		if (!this._onAdSizeChangeCallback) {
			return;
		}

		const view: HTMLCanvasElement = GameApp.pixi.view;
		if (view.width !== Math.floor(view.clientWidth * window.devicePixelRatio) ||
			view.height !== Math.floor(view.clientHeight * window.devicePixelRatio)) {
			this._onAdSizeChangeCallback({
				width: view.clientWidth,
				height: view.clientHeight
			});
		}
	}

}
