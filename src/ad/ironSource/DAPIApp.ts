import {IAdSizeState} from "../interfaces";
import {DAPIEvent, IDAPIApp} from "./interfaces";
import {AdApp} from "../AdApp";

export class DAPIApp extends AdApp {

	private _dapi: IDAPIApp = null;

	constructor() {
		super();
		window.onload = () => {
			this._dapi = window["dapi"];
			if (!this._dapi) {
				return;
			} else if (this._dapi.isReady()) {
				this._onDAPIReady();
			} else {
				this._dapi.addEventListener(DAPIEvent.READY, this._onDAPIReady.bind(this));
			}
		};
	}

	public isReady(): boolean {
		return !!this._dapi ? this._dapi.isReady() : false;
	}

	public isViewable(): boolean {
		return !!this._dapi ? this._dapi.isViewable() : false;
	}

	public isAudioEnabled(): boolean {
		return !!this._dapi && !!this._dapi.getAudioVolume();
	}

	public getScreenSize(): IAdSizeState {
		return !!this._dapi ? this._dapi.getScreenSize() : null;
	}

	public onButtonCTAClick(data?: any): void {
		if (!!this._dapi) {
			this._dapi.openStoreUrl();
		}
	}

	private _onDAPIReady(): void {
		this._dapi.removeEventListener(DAPIEvent.READY, this._onDAPIReady.bind(this));

		this._dapi.addEventListener(DAPIEvent.VIEWABLE_CHANGE, this._onDAPIViewableChange.bind(this));
		this._dapi.addEventListener(DAPIEvent.AUDIO_VOLUME_CHANGED, this._onDAPIAudioVolumeChange.bind(this));
		this._dapi.addEventListener(DAPIEvent.AD_RESIZE_EVENT, this.onDAPIAdResize.bind(this));

		if (!!this._onAdReadyCallback) {
			this._onAdReadyCallback();
		}
	}

	private _onDAPIViewableChange(event: any): void {
		if (!!this._onViewableChangeCallback) {
			this._onViewableChangeCallback({
				visible: event.isViewable
			});
		}
	}

	private _onDAPIAudioVolumeChange(event: any): void {
		if (!!this._onAdAudioVolumeChangeCallback) {
			this._onAdAudioVolumeChangeCallback({
				volume: parseInt(event, 10)
			});
		}
	}

	private onDAPIAdResize(event): void {
		if (!!this._onAdSizeChangeCallback) {
			this._onAdSizeChangeCallback(event);
		}
	}

}
