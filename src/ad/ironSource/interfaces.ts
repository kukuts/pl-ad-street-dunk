export enum DAPIEvent {
	READY = "ready",
	VIEWABLE_CHANGE = "viewableChange",
	AUDIO_VOLUME_CHANGED = "audioVolumeChange",
	AD_RESIZE_EVENT = "adResized"
}

export interface IDAPIApp {
	isReady(): boolean;

	isViewable(): boolean;

	getScreenSize(): any;

	getAudioVolume(): number; // 0 - 100, 0 = mute, 100 = sound on

	openStoreUrl(): void;

	audioVolumeChange(listener: (e: any) => void): void;

	addEventListener(event: string, listener: (e: any) => void): void;

	removeEventListener(event: string, listener: (e: any) => void): void;
}
