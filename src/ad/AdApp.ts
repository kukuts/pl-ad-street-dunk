import {
	AdSizeStateCallback,
	AdSoundStateCallback,
	AdViewStateCallback,
	AdVoidCallback,
	IAdApp,
	IAdSizeState
} from "./interfaces";

export abstract class AdApp implements IAdApp {

	protected _onAdReadyCallback: AdVoidCallback = null;
	protected _onAdAudioVolumeChangeCallback: AdSoundStateCallback = null;
	protected _onViewableChangeCallback: AdViewStateCallback = null;
	protected _onAdSizeChangeCallback: AdSizeStateCallback = null;

	abstract getScreenSize(): IAdSizeState;

	abstract isAudioEnabled(): boolean;

	abstract isReady(): boolean;

	abstract isViewable(): boolean;

	abstract onButtonCTAClick(data: any): void;

	public setOnAdReadyCallback(callback: () => void) {
		this._onAdReadyCallback = callback;
	}

	public setOnAdAudioVolumeChangeCallback(callback: AdSoundStateCallback): void {
		this._onAdAudioVolumeChangeCallback = callback;
	}

	public setOnViewableChangeCallback(callback: AdViewStateCallback): void {
		this._onViewableChangeCallback = callback;
	}

	public setOnAdSizeChangeCallback(callback: AdSizeStateCallback): void {
		this._onAdSizeChangeCallback = callback;
	}

}