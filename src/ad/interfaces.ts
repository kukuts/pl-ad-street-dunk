export type AdVoidCallback = () => void;
export type AdSoundStateCallback = (state: IAdSoundState) => void;
export type AdViewStateCallback = (state: IAdViewState) => void;
export type AdSizeStateCallback = (state: IAdSizeState) => void;

export interface IAdApp {
	setOnAdReadyCallback(callback: AdVoidCallback)

	setOnViewableChangeCallback(callback: AdViewStateCallback): void;

	setOnAdAudioVolumeChangeCallback(callback: AdSoundStateCallback): void;

	setOnAdSizeChangeCallback(callback: AdSizeStateCallback): void;

	onButtonCTAClick(data?: any): void;

	isReady(): boolean;

	isViewable(): boolean;

	isAudioEnabled(): boolean;

	getScreenSize(): IAdSizeState;
}

export interface IAdSizeState {
	width: number,
	height: number
}

export interface IAdViewState {
	visible: boolean
}

export interface IAdSoundState {
	volume: number
}