import Container = PIXI.Container;
import GameEndCardView from "./endCard/GameEndCardView";
import {GameApp} from "./GameApp";
import Resources from "./Resources";
import {getScreenOrientation, ScreenOrientation} from "../utils";

export default class GameView extends Container {

	private _endCardView: GameEndCardView = null;

	public async init() {
		await Resources.loadAssets();
		this._endCardView = new GameEndCardView();
		this.addChild(this._endCardView);
		this.layout();
	}

	public pause(): void {
		this._endCardView.ball.animation.pause();
	}

	public resume(): void {
		this._endCardView.ball.animation.resume();
	}

	public layout(): void {
		if (!!this._endCardView) {
			const orientation: ScreenOrientation = getScreenOrientation({
				width: GameApp.pixi.renderer.width,
				height: GameApp.pixi.renderer.height
			});

			if (this._endCardView.getLayoutOrientation() !== orientation) {
				this._endCardView.scale.set(1, 1);
				this._endCardView.setLayoutOrientation(orientation);
			}

			this._endCardView.scale.set(GameApp.scaleFactor, GameApp.scaleFactor);
			this._endCardView.x = GameApp.pixi.view.width * 0.5;
			this._endCardView.y = GameApp.pixi.view.height * 0.5;
		}
	}

}
