import * as assets from "assets/assets.json";

export enum ImageAsset {
	LOGO = "Logo",
	BALL = "Ball",
	CTA = "CTAButton",
}

export default class Resources {

	public static readonly loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();

	public static async loadAssets() {
		this.loader.add(ImageAsset.LOGO, assets[ImageAsset.LOGO]);
		this.loader.add(ImageAsset.BALL, assets[ImageAsset.BALL]);
		this.loader.add(ImageAsset.CTA, assets[ImageAsset.CTA]);
		await new Promise(resolve => Resources.loader.load(resolve));
	}

}
