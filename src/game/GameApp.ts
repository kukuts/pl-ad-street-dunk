import GameView from "./GameView";
import {IAdApp, IAdSizeState, IAdSoundState, IAdViewState} from "../ad/interfaces";
import {getScreenOrientation, ScreenOrientation} from "../utils";
import ApplicationOptions = PIXI.ApplicationOptions;
import {DESIGN_RESOLUTION} from "../constants";

export class GameApp {

	public static scaleFactor: number = 1;

	public static adApp: IAdApp = null;
	public static pixi: PIXI.Application = null;

	private readonly _gameView: GameView = null;

	constructor(adApp: IAdApp) {
		if (!adApp) {
			throw new Error("Ad app is null");
		}

		// Init pixi
		PIXI.utils.skipHello();
		GameApp.pixi = new PIXI.Application({
			view: document.getElementById("app-canvas")
		} as ApplicationOptions);
		this._gameView = new GameView();
		GameApp.pixi.stage.addChild(this._gameView);
		GameApp.pixi.renderer.backgroundColor = 0x303030;

		// Init ad app
		GameApp.adApp = adApp;
		if (GameApp.adApp.isReady()) {
			this._onAdReady();
		} else {
			GameApp.adApp.setOnAdReadyCallback(this._onAdReady.bind(this));
		}
	}

	private _onAdReady(): void {
		if (GameApp.adApp.isViewable()) {
			this._setMute(GameApp.adApp.isAudioEnabled());
			this.layout(GameApp.adApp.getScreenSize());
			this._gameView.init();
		}

		GameApp.adApp.setOnViewableChangeCallback(this._onAdViewableChange.bind(this));
		GameApp.adApp.setOnAdAudioVolumeChangeCallback(this._onAdAudioVolumeChange.bind(this));
		GameApp.adApp.setOnAdSizeChangeCallback(this._onAdSizeChange.bind(this));
	}

	private _onAdViewableChange(state: IAdViewState): void {
		if (state.visible) {
			this._gameView.resume();
			this.layout(GameApp.adApp.getScreenSize());
		} else {
			this._gameView.pause();
		}
	}

	private _onAdAudioVolumeChange(state: IAdSoundState): void {
		// Handle audio volume change
	}

	private _onAdSizeChange(state: IAdSizeState): void {
		this.layout(state);
	}

	private _setMute(mute: boolean): void {
		// Handle audio mute change
	}

	private layout(state: IAdSizeState): void {
		GameApp.pixi.renderer.resize(state.width, state.height);

		const orientation: ScreenOrientation = getScreenOrientation({
			width: GameApp.pixi.renderer.width,
			height: GameApp.pixi.renderer.height
		});

		const designWidth: number = DESIGN_RESOLUTION[orientation].width;
		const designHeight: number = DESIGN_RESOLUTION[orientation].height;

		GameApp.scaleFactor = Math.min(
			GameApp.pixi.view.clientWidth / designWidth,
			GameApp.pixi.view.clientHeight / designHeight
		);

		this._gameView.layout();
	}

}
