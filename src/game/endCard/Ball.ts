import Sprite = PIXI.Sprite;
import {TimelineMax, Circ} from "gsap";
import {BALL_FALL_DISTANCE} from "../../constants";

export default class Ball extends Sprite {

	private readonly _sprite: Sprite = null;
	private readonly _animation: TimelineMax = null;

	constructor(assetId: string) {
		super();

		this._sprite = Sprite.fromImage(assetId);
		this._sprite.anchor.set(0.5, 0.5);
		this.addChild(this._sprite);

		this._animation = this._createFallAnimation();
	}

	public get animation(): TimelineMax {
		return this._animation;
	}

	private _createFallAnimation(): TimelineMax {
		const animation: TimelineMax = new TimelineMax({repeat: -1});

		animation.to(
			this._sprite, 0.50,
			{
				y: BALL_FALL_DISTANCE,
				ease: Circ.easeIn,
				yoyo: true,
			}
		).to(
			this._sprite, 0.55,
			{
				y: 0,
				ease: Circ.easeOut,
				yoyo: true,
			}
		);


		return animation;
	}

}
