import Sprite = PIXI.Sprite;
import {PointerEvent} from "../../interfaces";
import {GameApp} from "../GameApp";
export default class CTAButton extends Sprite {

	private readonly _sprite: Sprite = null;

	constructor(assetId: string) {
		super();

		this._sprite = Sprite.fromImage(assetId);
		this.interactive = true;
		this.addChild(this._sprite);
		this.anchor.set(0.5, 0.5);

		this.on(PointerEvent.DOWN, this._onPointerEvent, this);
		this.on(PointerEvent.UP, this._onPointerEvent, this);
		this.on(PointerEvent.UP_OUTSIDE, this._onPointerEvent, this);
		this.on(PointerEvent.OVER, this._onPointerEvent, this);
	}

	private _onPointerEvent(event: any): void {
		switch (event.type) {
			case PointerEvent.DOWN:
				this.scale.set(0.95, 0.95);
				break;
			case PointerEvent.UP:
				GameApp.adApp.onButtonCTAClick();
				this.scale.set(1, 1);
				break;
			case PointerEvent.UP_OUTSIDE:
				this.scale.set(1, 1);
				break;
		}
	}

}
