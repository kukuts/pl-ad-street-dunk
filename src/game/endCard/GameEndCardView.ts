import Sprite = PIXI.Sprite;
import Container = PIXI.Container;
import {ScreenOrientation} from "../../utils";
import CTAButton from "./CTAButton";
import Ball from "./Ball";
import {ImageAsset} from "../Resources";

export default class GameEndCardView extends Container {

	private readonly _logo: Sprite = null;
	private readonly _ball: Ball = null;
	private readonly _cta: CTAButton = null;

	private readonly _layouts: any = {};

	private _layoutOrientation: ScreenOrientation = ScreenOrientation.Portrait;

	constructor() {
		super();

		this._logo = Sprite.fromImage(ImageAsset.LOGO);
		this._ball = new Ball(ImageAsset.BALL);
		this._cta = new CTAButton(ImageAsset.CTA);

		let layout: any = {};
		layout[ImageAsset.LOGO] = new PIXI.Point(310, 0);
		layout[ImageAsset.BALL] = new PIXI.Point(140, 170);
		layout[ImageAsset.CTA] = new PIXI.Point(490, 350);
		this._layouts[ScreenOrientation.Landscape] = layout;

		layout = {};
		layout[ImageAsset.LOGO] = new PIXI.Point(0, 0);
		layout[ImageAsset.BALL] = new PIXI.Point(430, 470);
		layout[ImageAsset.CTA] = new PIXI.Point(180, 800);
		this._layouts[ScreenOrientation.Portrait] = layout;

		this.addChild(this._logo);
		this.addChild(this._ball);
		this.addChild(this._cta);

		this.setLayoutOrientation(ScreenOrientation.Landscape);
	}

	public setLayoutOrientation(orientation: ScreenOrientation): void {
		if (this._layoutOrientation !== orientation) {
			const layout: any = this._layouts[orientation];

			this._logo.position.copy(layout[ImageAsset.LOGO]);
			this._ball.position.copy(layout[ImageAsset.BALL]);
			this._cta.position.copy(layout[ImageAsset.CTA]);

			this.pivot.set(this.width * 0.5, this.height * 0.5);

			this._layoutOrientation = orientation;
		}
	}

	public getLayoutOrientation(): ScreenOrientation {
		return this._layoutOrientation;
	}

	public get logo(): Sprite {
		return this._logo;
	}

	public get ball(): Ball {
		return this._ball;
	}

	public get CTAButton(): CTAButton {
		return this._cta;
	}

}
