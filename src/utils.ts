export enum ScreenOrientation {
	Landscape = "landscape",
	Portrait = "portrait"
}

export interface Dimensions {
	width: number,
	height: number
}

export function getScreenOrientation(dimensions: Dimensions): ScreenOrientation {
	return dimensions.width > dimensions.height
		? ScreenOrientation.Landscape
		: ScreenOrientation.Portrait;
}
