import {ScreenOrientation} from "./utils";

export const DESIGN_RESOLUTION: any = {};
DESIGN_RESOLUTION[ScreenOrientation.Landscape] = {
	width: 1200,
	height: 550
};
DESIGN_RESOLUTION[ScreenOrientation.Portrait] = {
	width: 900,
	height: 1000
};

export const BALL_FALL_DISTANCE: number = 200;

