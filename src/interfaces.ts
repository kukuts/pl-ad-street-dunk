export enum PointerEvent {
	DOWN = "pointerdown",
	UP = "pointerup",
	UP_OUTSIDE = "pointerupoutside",
	OVER = "pointerover",
}
